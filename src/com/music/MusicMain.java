package com.music;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.ListActivity;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MusicMain extends ListActivity implements View.OnClickListener {
    //for test
	private Button btn_ok;
	private Button btn_cancel;
	private ListView listview;

	// private String[] collect_artist;
	private ArrayList<String> collect_artist = new ArrayList<String>();

	private HashMap<String, Integer> rank_artist = new HashMap<String, Integer>();
	// private HashMap<String,Integer> test = new HashMap<String,Integer>();

	private static final String MEDIA_PATH = new String("/sdcard/");
	private ArrayList<Music> songs = new ArrayList<Music>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_main);

		btn_ok = (Button) findViewById(R.id.main_btn_ok);
		btn_cancel = (Button) findViewById(R.id.main_btn_cancel);
		listview = getListView();

		btn_ok.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
		updateSongList();
	}

	public void onClick(View v) {
		if (v == btn_ok) {

		} else if (v == btn_cancel) {
			finish();
		}
	}

	public void updateSongList() {
		// 원래는 10.
		Music[] music = new Music[1000];
		int cnt = 0;
		int cnt_temp = 0;
		Resources r = getResources();
		BitmapDrawable mDefaultAlbumIcon = (BitmapDrawable) r
				.getDrawable(R.drawable.git);

		String[] mCursorCols = new String[] { MediaStore.Audio.Media._ID,
				MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.TITLE,
				MediaStore.Audio.Media.ALBUM };

		Cursor cur = getContentResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, mCursorCols, null,
				null, null);

		if (cur.moveToFirst()) {

			String title, artist, album;
			Drawable d;

			int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
			int artistColumn = cur
					.getColumnIndex(MediaStore.Audio.Media.ARTIST);
			int albumColumn = cur.getColumnIndex(MediaStore.Audio.Media.ALBUM);
			
			do {
				title = cur.getString(titleColumn);
				artist = cur.getString(artistColumn);
				album = cur.getString(albumColumn);
				
				
				System.out.println(artist);
				if (collect_artist.contains(artist)) {
					// rank_artist.notify();
					rank_artist.put(artist, rank_artist.get(artist) + 1);
					System.out.println(rank_artist.get(artist));
				} else {
					rank_artist.put(artist, 1);
					collect_artist.add(cnt_temp++, artist);
				}

				/**
				 * collect_artist.add(cnt,artist);
				 * 
				 * if(collect_artist.contains(artist))
				 * System.out.println("It doesn't have this artist :" + artist);
				 */

				d = MusicUtils.getCachedArtwork(this, cnt + 1,
						mDefaultAlbumIcon);
			
				music[cnt] = new Music(artist, title, album, d);
				songs.add(music[cnt]);
				cnt++;
				// for test.
				if (cnt == 1000)
					break;
			} while (cur.moveToNext());
		}

		// for testing
		for (int i = 0; i < collect_artist.size(); i++) {
			System.out.println(collect_artist.get(i));
		}

		System.out.println("============\n\n\n");

		System.out.println(rank_artist.toString());
		/**
		 * for(int i = 0; i < rank_artist.size(); i++){ for(int j = i; j <
		 * rank_artist.size()-1; j++ ){ if(rank_artist.get(j) <
		 * rank_artist.get(j+1)){ Integer temp = rank_artist.get(j); } } }
		 */

		//
		MusicInformation songList = new MusicInformation(this, R.layout.row,
				songs);

		setListAdapter(songList);
		listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	@Override
	// ∏ÆΩ∫∆Æø°º≠ ∞Ó¿ª ≈¨∏Ø«ﬂ¿ª∂ß ¿Áª˝
	protected void onListItemClick(ListView l, View v, int position, long id) {
		File home = new File(MEDIA_PATH);
		String[] list = new String[10];
		list = home.list(new MP3Filter());
	}

	private class MusicInformation extends ArrayAdapter<Music> {
		private ArrayList<Music> items;

		public MusicInformation(Context context, int textViewResourceId,
				ArrayList<Music> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			View v = view;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.row, null);
			}
			Music m = items.get(position);
			if (m != null) {
				ImageView imageview = (ImageView) v
						.findViewById(R.id.row_album_art);
				TextView tt = (TextView) v.findViewById(R.id.row_artist);
				TextView bt = (TextView) v.findViewById(R.id.row_title);

				if (tt != null) {
					tt.setText(m.getGasu() + " - " + m.getJemok());
				}
				if (bt != null) {
					bt.setText(m.getAlbumName());
				}
				if (imageview != null) {
					imageview.setImageDrawable(m.getImage());
				}
			}
			return v;
		}
	}

	class Music {
		private String gasu;
		private String jemok;
		private String albumName;
		private Drawable drawimage;

		public Music(String _gasu, String _jemok, String _albumName,
				Drawable _image) {
			this.gasu = _gasu;
			this.jemok = _jemok;
			this.albumName = _albumName;
			this.drawimage = _image;
		}

		public String getGasu() {
			return gasu;
		}

		public String getJemok() {
			return jemok;
		}

		public String getAlbumName() {
			return albumName;
		}

		public Drawable getImage() {
			return drawimage;
		}
	}

	class MP3Filter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			return name.endsWith(".mp3");
		}
	}
}