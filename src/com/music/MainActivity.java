package com.music;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
//


@SuppressLint("NewApi")
public class MainActivity extends Activity implements OnClickListener{
	Button btnBook, btnMusic;
	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		btnBook = (Button)findViewById(R.id.btnBook);
		btnBook.setOnClickListener(this);
		btnMusic  = (Button)findViewById(R.id.btnMusic);
		btnMusic.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		if(v.getId() == btnBook.getId()){
			startActivity(new Intent("IntentBook"));
		}
		
		if(v.getId() == btnMusic.getId()){
			startActivity(new Intent("IntentMusic"));
		}
		
	}
}
